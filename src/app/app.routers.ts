import {Routes, RouterModule} from "@angular/router";
import {HeroesComponent} from "./heroes/heroes.component";
import {HERO_ROUTES} from "./heroes/hero.routes";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {DASHBOARD_ROUTES} from "./dashboard/dashboard.routes";

const APP_ROUTER: Routes = [
  { path: '', redirectTo: '/heroes', pathMatch: 'full'},
  { path: 'heroes', component: HeroesComponent, children: HERO_ROUTES},
  { path: 'dashboard', component: DashboardComponent, children: DASHBOARD_ROUTES}
];

export const routing = RouterModule.forRoot(APP_ROUTER);
