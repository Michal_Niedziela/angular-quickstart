import { Component, OnInit } from '@angular/core';
import {HeroesService} from "./heroes/heroes.service";

@Component({
  moduleId: module.id,
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  constructor(private heroesService: HeroesService) { }

  ngOnInit() {
  }

  onStore(){
    this.heroesService.storeData().subscribe(
      data => console.log(data),
      error => console.log(error)
    )
  }

  onFetch(){
    this.heroesService.fetchData();
  }

}
