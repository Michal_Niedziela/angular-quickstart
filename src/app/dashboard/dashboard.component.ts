import { Component, OnInit } from '@angular/core';
import {HeroesService} from "../heroes/heroes.service";
import {Hero} from "../shared/hero";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  heroes: Hero[] = [];

  constructor(private heroesService: HeroesService, private router: Router) { }

  ngOnInit() {
    this.heroes = this.heroesService.getHeroes().slice(0,4);
  }

  gotoDetail(index: number){
    this.router.navigate(['/dashboard',index]);
  }

}
