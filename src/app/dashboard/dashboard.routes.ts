import {RouterConfig} from "@angular/router";
import {DashboardStartComponent} from "./dashboard-start.component";
import {HeroDetailComponent} from "../heroes/hero-detail/hero-detail.component";
import {DashboardDetailComponent} from "./dashboard-detail.component";
export const DASHBOARD_ROUTES: RouterConfig = [
  { path: '', component: DashboardStartComponent},
  { path: ':id', component: DashboardDetailComponent},
];
