import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Hero} from "../shared/hero";
import {Subscription} from "rxjs/Rx";
import {HeroesService} from "../heroes/heroes.service";
import {Router, ActivatedRoute} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-dashboard-detail',
  templateUrl: 'dashboard-detail.component.html',
  styles: []
})
export class DashboardDetailComponent implements OnInit, OnDestroy {

  @Input() selectedHero: Hero;
  private heroIndex:number;
  private subscription: Subscription;

  constructor(private heroesService: HeroesService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      (params:any) => {
        this.heroIndex = params['id'];
        this.selectedHero = this.heroesService.getHero(this.heroIndex);
      }
    )
  }
  
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
