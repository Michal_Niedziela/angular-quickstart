import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-hero-start',
  template: `
    <p>
      Choose Hero!
    </p>
  `,
  styles: []
})
export class HeroStartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
