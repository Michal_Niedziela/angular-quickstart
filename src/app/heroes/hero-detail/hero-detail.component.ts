import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Hero} from "../../shared/hero";
import {HeroesService} from "../heroes.service";
import {Router, ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs/Rx";


@Component({
  moduleId: module.id,
  selector: 'app-hero-detail',
  templateUrl: 'hero-detail.component.html',
  styleUrls: ['hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit, OnDestroy {
  @Input() selectedHero: Hero;
  private heroIndex:number;
  private subscription: Subscription;

  constructor(private heroesService: HeroesService,
  private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      (params:any) => {
        this.heroIndex = params['id'];
        this.selectedHero = this.heroesService.getHero(this.heroIndex);
      }
    )
  }

  onEdit(){
    this.router.navigate(['/heroes', this.heroIndex, 'edit'])
  }

  onDelete(){
    this.heroesService.deleteHero(this.selectedHero);
    this.router.navigate(['/heroes']);
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
