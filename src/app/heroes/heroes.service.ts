import { Injectable, EventEmitter} from '@angular/core';
import {Hero} from "../shared/hero";
import {Power} from "../shared/power";
import {Http, Headers, Response} from "@angular/http";

@Injectable()
export class HeroesService {
  heroesChanged = new EventEmitter();

  private heroes: Hero[] = [
    new Hero('Mr. Nice', 'Very nice', [new Power('kindly'), new Power('nice')]),
    new Hero('Narco','Strange', [new Power('flying'),new Power('fearing')]),
    new Hero('Bombasto','Very heavy',[new Power('Strong')]),
    new Hero('Mr. Robot', 'Just robot', [new Power('Metalic'), new Power('Extra armor')]),
    new Hero('Vision','Can see everything', [new Power('super seeing')]),
    new Hero('Guma','Gumowy facet',[new Power('flexibility')])
  ];

  constructor(private http: Http) { }

  getHeroes(): Hero[] {
    return this.heroes;
  }

  getHero(heroId: number) {
    return this.heroes[heroId];
  }

  addHero(hero: Hero){
    this.heroes.push(hero);
  }

  editHero(oldHero: Hero, newHero: Hero){
    this.heroes[this.heroes.indexOf(oldHero)] = newHero;
  }

  deleteHero(hero: Hero){
    this.heroes.splice(this.heroes.indexOf(hero),1);
  }

  storeData(){
    const body = JSON.stringify(this.heroes);
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.put('https://heroes-8667f.firebaseio.com/heroes.json', body, {headers: headers});
  }

  fetchData(){
    return this.http.get('https://heroes-8667f.firebaseio.com/heroes.json')
      .map((response: Response) => response.json())
      .subscribe(
        (data: Hero[]) => {
          this.heroes = data;
          this.heroesChanged.emit(this.heroes);
        }
      )
  }

}
