import {RouterConfig} from "@angular/router";
import {HeroStartComponent} from "./hero-start.component";
import {HeroDetailComponent} from "./hero-detail/hero-detail.component";
import {HeroEditComponent} from "./hero-edit/hero-edit.component";
export const HERO_ROUTES: RouterConfig = [
  { path: '', component: HeroStartComponent},
  { path: 'new', component: HeroEditComponent},
  { path: ':id', component: HeroDetailComponent},
  { path: ':id/edit', component: HeroEditComponent}
];
