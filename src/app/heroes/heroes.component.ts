import { Component, OnInit } from '@angular/core';
import {Hero} from "../shared/hero";

@Component({
  moduleId: module.id,
  selector: 'app-heroes',
  templateUrl: 'heroes.component.html',
  styleUrls: ['heroes.component.css']
})
export class HeroesComponent implements OnInit {

  selectedHero: Hero;
  constructor() { }

  ngOnInit() {
  }

}
