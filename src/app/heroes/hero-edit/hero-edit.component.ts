import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormGroup, FormBuilder, FormArray, Validators, FormControl} from "@angular/forms";
import {Subscription} from "rxjs/Rx";
import {Hero} from "../../shared/hero";
import {ActivatedRoute, Router} from "@angular/router";
import {HeroesService} from "../heroes.service";

@Component({
  moduleId: module.id,
  selector: 'app-hero-edit',
  templateUrl: 'hero-edit.component.html',
  styleUrls: ['hero-edit.component.css']
})
export class HeroEditComponent implements OnInit, OnDestroy {

  private heroIndex:number;
  private isNew = true;
  heroForm:FormGroup;
  private subscription:Subscription;
  private hero:Hero;

  constructor(private route:ActivatedRoute,
              private formBuilder:FormBuilder,
              private heroesService:HeroesService,
              private router:Router) {
  }

  ngOnInit() {

    this.subscription = this.route.params.subscribe(
      (params:any) => {
        if(params.hasOwnProperty('id')) {
          this.isNew = false;
          this.heroIndex = +params['id'];
          this.hero = this.heroesService.getHero(this.heroIndex);
        } else {
          this.isNew = true;
          this.hero = null;
        }
        this.initForm();
      }
    );
    console.log('Check news',this.isNew);
  }

  private initForm() {
    let heroName = '';
    let heroDescription = '';
    let heroPowers:FormArray = new FormArray([]);

    if(!this.isNew){
      if(this.hero.hasOwnProperty('powers')) {
        for(let i = 0; i < this.hero.powers.length; i++){
          heroPowers.push(
            new FormGroup({
              name: new FormControl(this.hero.powers[i].name, Validators.required)
            })
          );
        }
      }
      heroName = this.hero.name;
      heroDescription = this.hero.description;
    }

    this.heroForm = this.formBuilder.group({
      name: [heroName, Validators.required],
      description: [heroDescription, Validators.required],
      powers: heroPowers
    });
    console.log(this.isNew);
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
  
  onSubmit(){
    const newHero = this.heroForm.value;
    if(this.isNew){
      this.heroesService.addHero(newHero);
    }else{
      this.heroesService.editHero(this.hero, newHero);
    }
    this.navigateBack();
  }
  
  addPower(name: string){
    (<FormArray>this.heroForm.controls['powers']).push(
      new FormGroup({
        name: new FormControl(name, Validators.required)
      })
    );
  }
  
  removePower(index: number){
    (<FormArray>this.heroForm.controls['powers']).removeAt(index);
  }
  
  private navigateBack(){
    this.router.navigate(['../']);
  }
  
  onCancel(){
    this.navigateBack();
  }

}
