import { Component, OnInit } from '@angular/core';
import {Hero} from "../../shared/hero";
import { HeroesService} from '../heroes.service'

@Component({
  moduleId: module.id,
  selector: 'app-hero-list',
  templateUrl: 'hero-list.component.html',
  styles: [`
.button-new {
  font-family: Arial;
  color: #3d3d3d; 
  background-color: #7b99a7;
  border: none;
  padding: 5px 10px;
  border-radius: 4px;
  cursor: pointer;
  cursor: hand;
}
.button-new:hover {
  background-color: #cfd8dc;
}
.button-new:disabled {
  background-color: #eee;
  color: #aaa;
  cursor: auto;
}`]
})
export class HeroListComponent implements OnInit {

  heroes : Hero[] = [];
  constructor(private heroService: HeroesService) { }

  ngOnInit() {
    this.heroes = this.heroService.getHeroes();
    this.heroService.heroesChanged.subscribe(
      (heroes: Hero[]) => this.heroes = heroes
    )
  }

}
