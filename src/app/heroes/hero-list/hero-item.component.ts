import { Component, OnInit , Input} from '@angular/core';
import {Hero} from "../../shared/hero";

@Component({
  moduleId: module.id,
  selector: 'app-hero-item',
  templateUrl: 'hero-item.component.html',
  styles: [`
.module {
	padding: 20px;
	text-align: center;
	color: #eee;
	max-height: 120px;
	min-width: 120px;
	background-color: #607D8B;
	border-radius: 2px;
}
h4 {
  position: relative;
}
.module:hover {
  background-color: #EEE;
  cursor: pointer;
  color: #607d8b;
}
`]
})
export class HeroItemComponent implements OnInit {
  @Input() hero: Hero;
  @Input() heroId: number;
  
  constructor() { }

  ngOnInit() {
  }

}
