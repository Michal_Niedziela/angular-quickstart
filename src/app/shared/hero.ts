import {Power} from "./power";
export class Hero {
  name: string;
  description: string;
  powers: Power[];

  constructor( name: string, description: string, powers: Power[]){
    this.name = name;
    this.description = description;
    this.powers = powers;
  }
}
