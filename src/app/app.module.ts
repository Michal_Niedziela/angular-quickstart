/**
 * Created by sunday on 8/22/16.
 */
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from "./app.component";

import {routing} from "./app.routers";
import {HeroesService} from "./heroes/heroes.service";
import {HeroDetailComponent} from "./heroes/hero-detail/hero-detail.component";
import {HeroListComponent} from "./heroes/hero-list/hero-list.component";
import {HeroItemComponent} from "./heroes/hero-list/hero-item.component";
import {HeroesComponent} from "./heroes/heroes.component";
import { DropdownDirective } from './dropdown.directive'
import {HeaderComponent} from "./header.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {DashboardStartComponent} from "./dashboard/dashboard-start.component";
import {HttpModule} from "@angular/http";
//import { DATEPICKER_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    HeroDetailComponent,
    HeroListComponent,
    HeroItemComponent,
    DropdownDirective,
    HeaderComponent,
    DashboardComponent,
    DashboardStartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, // For template-driven approach
    ReactiveFormsModule, // For data-driven approach
    routing,
    HttpModule
  ],
  bootstrap: [AppComponent],
  providers: [HeroesService]
})
export class AppModule {}
